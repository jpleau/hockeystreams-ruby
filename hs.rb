# TODO: Add a way to list current games if no gameId passed as parameter (and allow passing gameId as parameter..)
# Requires VLC and rtmpdump to be in $PATH
# apt-get install vlc rtmpdump
# If you're on a mac you should know what to do, who cares about macs anyway

require "json"
require "net/https"

# The stream you want to watch, for example: 
# http://www4.hockeystreams.com/live-streams/new+york+islanders-vs-florida+panthers-03-02-2014/17567
# gameId would be 17567

gameId = 00000

config = {
	# Enter your hockeystreams username
	username: "",

	# Enter your hockeystreams password
	password: "", 

	# Enter your hockeystreams API key (Generate one at https://www4.hockeystreams.com/api)
	key: "", 

  # not needed, if you don't have a token the script will generate one
	token: ""
}

http = Net::HTTP.new("api.hockeystreams.com")

if config[:token].empty?
	request = Net::HTTP::Post.new("/Login")
	request.set_form_data(config)
	response = http.request(request)
	str = JSON.parse(response.body)
	config[:token] = str["token"]
end

request = Net::HTTP::Get.new("/GetLiveStream?token=#{config[:token]}&id=#{gameId}")
response = http.request(request)

gameInfo = JSON.parse(response.body)
liveURL = gameInfo["TrueLiveHD"]

if !liveURL.empty?
	rtmp = liveURL[0]["src"]
	cmd = "rtmpdump -i \"#{rtmp}\" -o - | vlc fd://0"
	%x(#{cmd})
end
